#!/bin/bash

source ../settings.sh

echo "Running hello world server at http://localhost:5000."
echo

docker run --rm --name $CONTAINERNAME --user "$(id -u):$(id -g)" --env LEKTOR_OUTPUT_PATH=../htdocs --volume "${PWD}":/opt/lektor --publish 5000:5000 $IMAGENAME --project hello-world server --host 0.0.0.0
