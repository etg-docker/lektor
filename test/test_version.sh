#!/bin/bash

source ../settings.sh

echo "Shows version."
echo

docker run --rm --name $CONTAINERNAME $IMAGENAME --version
