#!/bin/bash

source ../settings.sh

echo "Running hello world build into htdocs."
echo

# clean files, then build new
docker run --rm --name $CONTAINERNAME --user "$(id -u):$(id -g)" --env LEKTOR_OUTPUT_PATH=../htdocs --volume "${PWD}":/opt/lektor --publish 5000:5000 $IMAGENAME --project hello-world clean --yes
docker run --rm --name $CONTAINERNAME --user "$(id -u):$(id -g)" --env LEKTOR_OUTPUT_PATH=../htdocs --volume "${PWD}":/opt/lektor --publish 5000:5000 $IMAGENAME --project hello-world build
