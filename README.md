# etg-docker: ekleinod/lektor

See the [Known problems](#known-problems) section (and the [issue tracker](https://gitlab.com/etg-docker/lektor/-/issues)) before use.

## Quick reference

- **Maintained by:** [ekleinod](https://gitlab.com/etg-docker/lektor/)
- **Docker hub:**: [ekleinod/lektor](https://hub.docker.com/r/ekleinod/lektor)
- **Where to get help:** [gitlab](https://gitlab.com/etg-docker/lektor/), [issue tracker](https://gitlab.com/etg-docker/lektor/-/issues)


## Supported tags

- `1.0.0`, `1.0.0-3.2.0`, `latest`


## What is this image?

This is a docker image for the static content management system lektor.
The image has the name `ekleinod/lektor`.
It is based on my python image `ekleinod/python`.

- [Lektor Homepage](https://www.getlektor.com)

This image is used mainly for my websites, thus the dependencies of these websites are added to python via pip.

The following plugins/packages are installed:

- `lektor-markdown-header-anchors`
- `lektor-jinja-content`

For the complete changelog, see [changelog.md](changelog.md)


## How to use the image

You can use the image as server on port 5000 as follows:

~~~ bash
$ docker run --rm --name <containername> --user "$(id -u):$(id -g)" --env LEKTOR_OUTPUT_PATH=<output path> --volume "${PWD}":/opt/lektor --publish 5000:5000 ekleinod/lektor --project <project path> server --host 0.0.0.0
~~~

Please note, that `/opt/lektor` is the work directory of the image.

For a simple test showing the lektor help page, run

~~~ bash
$ docker run --rm --name lektor ekleinod/lektor
~~~

You can see examples at work in the test folder.
Clone the repository or download the folder.
Call them as follows:

~~~ bash
$ cd test
$ ./test_help.sh
$ ./test_hello-world_server.sh
$ ./test_hello-world_build.sh
$ ./test_version.sh
~~~

`test_hello-world_server.sh` starts a webserver with the test website, you can access ist at <http://localhost:5000>. Stop it with Ctrl+c.

`test_hello-world_build.sh` builds the test website in the folder `htdocs` (for upload to a server).


## Known problems

See the [issue tracker](https://gitlab.com/etg-docker/lektor/-/issues) for more details and the current situation.

### Symlinks

The image does not work with symlinks.
So far I did not investigate so far if this is a docker problem or a lektor problem, and how deep this problem is rooted.

### Lektor packages

Due to the design of the extension mechanism it is very difficult to use project specific packages (plugins).

Lektor tries to install/cache the packages per project, thus it tries to do this with every call of the image.
This is time consuming and bad.

Furthermore, during the install it tries to write to a cache directory in the image, which does not work when calling the image with user credentials.


## Releases

The latest release will always be available with:

`ekleinod/lektor:latest`

There are two naming schemes:

1. `ekleinod/lektor:<internal>-<lektor>`

	Example: `ekleinod/lektor:1.0.0-3.2.0`

2. internal version number `ekleinod/lektor:<major>.<minor>.<patch>`

	Example: `ekleinod/lektor:1.0.0`


## Build locally

In order to build the image locally, clone the repository and call

~~~ bash
$ cd image
$ ./build_image.sh
~~~

## Git-Repository

The branching model regards to the stable mainline model described in <https://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git/>.

This means, there is always a stable mainline, the main branch.
This branch ist always compileable and testable, both without errors.

Features are developed using feature branches.
Special feature branches are used (different from the mainline model) for finalizing releases.

Releases are created as branches of the mainline.
Additionally, each release is tagged, tags contain the patch and, if needed, additional identifiers, such as `rc1` or `beta`.

Patches are made in the according release branch.
Minor version changes get their own release branch.

## Copyright

Copyright 2021-2021 Ekkart Kleinod <ekleinod@edgesoft.de>

The program is distributed under the terms of the GNU General Public License, either version 3 of the License, or
any later version.

See COPYING for details.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
